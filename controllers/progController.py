from models.shopModel import Item
from views import shopView
from models.accountancyModel import Worker
from views import accountancyView
from models import adminModel
from views import adminView


def start():
    print("""1. administrator panel
2. shop
3. stop""")
    choice = takeIntInput()
    if (choice == 1):
        startAdminView()
        checkPassword()
    elif (choice == 2):
        startShop()
    elif(choice == 3):
        pass
    else:
        print("WRONG INPUT, TRY AGAIN!")
        start()


def showAllItems():
    retData = Item.getAllItems()
    myItems = retData[0]
    status = retData[1]
    if(status == 200):
        return shopView.showItems(myItems)


def startShop():
    shopView.startView()
    showAllItems()
    totalPrice = 0
    inpValue = input("Want to buy anything?(yes/anything else)")
    while (inpValue == "yes"):
        if (inpValue == "yes"):
            shopView.buyItems()
            print("What's the ID of the item you want to buy")
            itemID = takeIntInput()
            print("what's the quantity you want to buy")
            itemQuantity = takeIntInput()
            if (itemQuantity > 0):
                returnedData = Item.buyItems(itemID, itemQuantity)
                status = returnedData[0]
                if(status == True):
                    print("Bought successfully")
                    totalPrice = totalPrice + returnedData[1]
                else:
                    print("invalid ID or quantity, be more careful")
                inpValue = input("want to keep buying(yes/anything else)?")
            else:
                print("invalid input, quantity must be greater than 0")
                continue
    print("YOU HAVE TO PAY " + str(totalPrice))
    start()


def showAll():
    myWorkers = Worker.getAllworkers()
    return accountancyView.showWorkers(myWorkers)


def startAccountacyView():
    accountancyView.startView()
    showAll()


def startAdminView():
    adminView.startView()


def checkPassword():
    pas = input()
    if (adminModel.isAdmin(pas)[1] == 201):
        takeAdminChoice()
    else:
        print("wrong password")
        start()


def takeAdminChoice():
    adminView.showAdminMenu()
    myChoice = takeIntInput()
    if (myChoice == 1):
        takeItemChoice()
    elif (myChoice == 2):
        takeWorkerChoice()
    elif (myChoice == 3):
        start()
    else:
        print("NO SUCH OPTION, TRY AGAIN")
        takeAdminChoice()


def takeItemChoice():
    adminView.showItemsMenu()
    myChoice = takeIntInput()
    if (myChoice == 1):
        addItem()
        takeAdminChoice()
    elif (myChoice == 2):
        editItem()
        takeAdminChoice()
    elif (myChoice == 3):
        deleteItem()
        takeAdminChoice()
    elif (myChoice == 4):

        start()
    else:
        print("WRONG INPUT TRY AGAIN!")
        takeItemChoice()


def takeWorkerChoice():
    adminView.showWorkersMenu()
    myChoice = takeIntInput()
    if (myChoice == 1):
        addWorker()
        takeAdminChoice()
    elif (myChoice == 2):
        editWorker()
        takeAdminChoice()
    elif (myChoice == 3):
        deleteWorker()
        takeAdminChoice()
    elif (myChoice == 4):
        start()
    else:
        print("wrong input")
        takeWorkerChoice()


def deleteItem():
    delID = int(input("Enter the item ID you want to delete"))
    status = Item.removeItem(delID)
    if (status == True):
        print("SUCCESS")
    else:
        print("NO SUCH WORKER TO EDIT")


def editItem():
    myID = int(input("Enter the item ID you want to edit"))
    newName = input("Enter the new name")
    print("Enter the new price")
    newPrice = takeIntInput()
    print("Enter the new quatity")
    newQuantity = takeIntInput()
    status = Item.editItem(myID, newName, newPrice, newQuantity)[1]
    if (status == 201):
        print("SUCCESS")
    else:
        print("NO SUCH ITEM TO EDIT")


def addItem():
    itemName = input("Enter the name of the item you want to add")
    print("Enter the price of the item")
    itemPrice = takeFloatInput()
    print("Enter the quantity of the item")
    itemQuantity = takeIntInput()
    item = Item.addItem(itemName, itemPrice, itemQuantity)[0]
    print("success")
    print(item)



def deleteWorker():
    delID = input("Enter the item ID you want to delete")
    status = Worker.removeWorker(delID)
    if (status == True):
        print("SUCCESS")
    else:
        print("NO SUCH WORKER TO REMOVE")


def editWorker():
    myID = input("Enter the item ID you want to edit")
    newName = input("Enter the new name")
    newLastName = input("Enter the new last name")
    print("Enter the new salary")
    newSalary = takeFloatInput()
    status = Worker.editWorker(myID, newName, newLastName, newSalary)
    if (status == True):
        print("SUCCESS")
    else:
        print("NO SUCH WORKER TO EDIT")


def addWorker():
    workerName = input("Enter the name of the worker you want to add")
    workerLastName = input("Enter the last name of the worker you want to add")
    print("Enter the worker's salary")
    workerSalary = takeFloatInput()
    worker = Worker.addWorker(workerName, workerLastName, workerSalary)[0]
    print("success")
    print(worker)


def takeIntInput():
    a = input("Enter a number")
    if (a.isdigit()):
        return int(a)
    print("WRONG INPUT,TRY AGAIN!")
    return takeIntInput()


def takeFloatInput():
    a = input("Enter a number(can be decimals too)")
    if (a.replace(".", "", 1).isdigit()):
        return float(a)
    print("WRONG INPUT,TRY AGAIN!")
    return takeFloatInput()

