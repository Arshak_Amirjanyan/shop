import json
import requests


def getJsonPassword():
    with open('shopDB') as shop_data:
        json_data = json.load(shop_data)
        return json_data["adminPassword"]


def isAdmin(pas):
    data = {"password": pas}
    resp = requests.post("http://127.0.0.1:5000/admin", data=data)
    resp = resp.json()
    return resp
