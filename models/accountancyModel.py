import json
import requests


class Worker:

    def __init__(self, id=None, firstName=None, lastName=None, salary=None):
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.salary = salary

    def getWorker(self):
        return self.id, self.firstName, self.lastName, self.salary

    @classmethod
    def getJsonItems(cls):
        items = []
        with open('shopDB') as shop_data:
            json_data = json.load(shop_data)

        return json_data

    @classmethod
    def generateID(cls):
        json_data = cls.getJsonItems()
        items_data = json_data["workers"]
        workerID = int(items_data[len(items_data) - 1]["id"]) + 1
        return workerID


    @classmethod
    def getAllWorkers(cls):
        resp = requests.get("http://127.0.0.1:5000/workers")
        resp = resp.json()
        return resp


    @classmethod
    def addWorker(cls, firstName, lastName, salary):
        workerID = cls.generateID()
        data = {"id": workerID, "firstName": firstName, "lastName": lastName, "salary": salary}
        resp = requests.post("http://127.0.0.1:5000/workers", data=data)
        resp = resp.json()
        return resp

    @classmethod
    def editWorker(cls, workerID, workerFirstName, workerLastName, workerSalary):
        data = {"id": workerID, "firstName": workerFirstName, "lastName": workerLastName, "salary": workerSalary}
        resp = requests.put("http://127.0.0.1:5000/items", data=data)
        resp = resp.json()
        return resp

    @classmethod
    def removeWorker(cls, workerID):
        status = False
        json_data = cls.getJsonItems()
        workers_data = json_data["workers"]
        for worker in workers_data:
            if (workerID == worker["id"]):
                status = workers_data.remove(worker)
        json_data["workers"] = workers_data
        with open('shopDB', "w") as data_file:
            json.dump(json_data, data_file)
        return status
