import json
import requests


class Item:

    def __init__(self, id=None, name=None, price=None, quantity=None):
        self.id = id
        self.name = name
        self.price = price
        self.quantity = quantity

    def getItem(self):
        return self.id, self.name, self.price, self.quantity

    @classmethod
    def getJsonItems(cls):
        items = []
        with open('shopDB') as shop_data:
            json_data = json.load(shop_data)

        return json_data

    @classmethod
    def buyItems(cls, itemID, quantity):
        status = False
        totalPrice = 0
        json_data = cls.getJsonItems()
        items_data = json_data["items"]
        for item in items_data:
            if (itemID == item["id"]):
                if (quantity <= item["quantity"] and quantity > 0):
                    item["quantity"] = item["quantity"] - quantity
                    status = True
                    totalPrice = item["price"] * quantity
        json_data["items"] = items_data
        with open('shopDB', "w") as data_file:
            json.dump(json_data, data_file)
            return status, totalPrice



    @classmethod
    def getAllItems(cls):
        resp = requests.get("http://127.0.0.1:5000/items")
        resp = resp.json()
        return resp


    @classmethod
    def generateID(cls):
        json_data = cls.getJsonItems()
        items_data = json_data["items"]
        itemID = int(items_data[len(items_data)-1]["id"]) + 1
        return itemID

    @classmethod
    def addItem(cls, itemName, itemPrice, itemQuantity):
        itemID = cls.generateID()
        data = {"id": itemID, "name": itemName, "price": itemPrice, "quantity": itemQuantity}
        resp = requests.post("http://127.0.0.1:5000/items", data=data)
        resp = resp.json()
        return resp

    @classmethod
    def editItem(cls, itemID, itemName, itemPrice, itemQuantity):
        data = {"id": itemID, "name": itemName, "price": itemPrice, "quantity": itemQuantity}
        resp = requests.put("http://127.0.0.1:5000/items", data=data)
        resp = resp.json()
        return resp


    @classmethod
    def removeItem(cls, itemID):
        status = False
        json_data = cls.getJsonItems()
        items_data = json_data["items"]
        for item in items_data:
            if (itemID == item["id"]):
                items_data.remove(item)
                status = True
        json_data["items"] = items_data
        with open('shopDB', "w") as data_file:
            json.dump(json_data, data_file)
        return status
