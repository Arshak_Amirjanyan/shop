from flask import Flask
from flask_restful import Api, Resource, reqparse
import json

app = Flask(__name__)
api = Api(app)


class API(Resource):

    def get(self, name):
        with open('shopDB') as shop_data:
            data = json.load(shop_data)
            myKeys = data.keys()
            for key in myKeys:
                if(name == key):
                    myData = data[name]
                    return [myData, 200]
            return ["not found", 404]

    def post(self, name):
        if (name == "admin"):
            parser = reqparse.RequestParser()
            parser.add_argument("password")
            args = parser.parse_args()
            with open('shopDB') as shop_data:
                data = json.load(shop_data)
                myData = data["adminPassword"]
                if (args["password"]) == myData:
                    return [True, 201]
                return [False, 404]

        if (name == "items"):
            parser = reqparse.RequestParser()
            parser.add_argument("id")
            parser.add_argument("name")
            parser.add_argument("price")
            parser.add_argument("quantity")
            args = parser.parse_args()
            with open('shopDB') as shop_data:
                data = json.load(shop_data)
                myData = data["items"]
                newItem = {"id": int(args["id"]), "name": args["name"], "price": float(args["price"]), "quantity": int(args["quantity"])}
                myData.append(newItem)
                data["items"] = myData
                with open('shopDB', "w") as data_file:
                    json.dump(data, data_file)
                    return [newItem, 201]

        if (name == "workers"):
            parser = reqparse.RequestParser()
            parser.add_argument("id")
            parser.add_argument("firstName")
            parser.add_argument("lastName")
            parser.add_argument("salary")
            args = parser.parse_args()
            with open('shopDB') as shop_data:
                fullData = json.load(shop_data)
                workData = fullData["workers"]
                newWorker = {"id": int(args["id"]), "firstName": args["firstName"], "lastName": args["lastName"],
                             "salary": float(args["salary"])}
                workData.append(newWorker)
                fullData["workers"] = workData
                with open('shopDB', "w") as data_file:
                    json.dump(fullData, data_file)
                    return [newWorker, 201]

        else:
            return ["not found", 404]

    @classmethod
    def put(cls, linkName, idNum):
        if (linkName == "items"):
            parser = reqparse.RequestParser()
            parser.add_argument("id")
            parser.add_argument("name")
            parser.add_argument("price")
            parser.add_argument("quantity")
            args = parser.parse_args()
            with open('shopDB') as shop_data:
                data = json.load(shop_data)
                myData = data["items"]
                for item in myData:
                    if (idNum == item["id"]):
                        item["name"] = args["name"]
                        item["price"] = float(args["price"])
                        item["quantity"] = int(args["quantity"])
                        data["items"] = myData
                        with open('shopDB', "w") as data_file:
                            json.dump(data, data_file)
                            return [item, 200]
                return ["Not found", 404]

        if(linkName == "workers"):
            parser = reqparse.RequestParser()
            parser.add_argument("id")
            parser.add_argument("firstName")
            parser.add_argument("lastName")
            parser.add_argument("salary")
            args = parser.parse_args()
            with open('shopDB') as shop_data:
                data = json.load(shop_data)
                myData = data["workers"]
                for worker in myData:
                    if (idNum == worker["id"]):
                        worker["firstName"] = args["firstName"]
                        worker["lastName"] = args["lastName"]
                        worker["salary"] = args["salary"]
                        data["workers"] = myData
                        with open('shopDB', "w") as data_file:
                            json.dump(data, data_file)
                            return [worker, 200]
                return ["Not found", 404]

    @classmethod
    def delete(cls, linkName, idNum):
            with open('shopDB') as shop_data:
                data = json.load(shop_data)
                myData = data[linkName]
                for d in myData:
                    if(d["id"] == idNum):
                        myData.remove(d)
                        data[linkName] = myData
                        with open('shopDB', "w") as data_file:
                            json.dump(data, data_file)
                            return [d, 200]
                return ["Not Found", 404]


api.add_resource(API, "/<string:linkName>/<int:idNum>", "/<string:name>")

app.run(debug=True)

